FROM easye/golang:1.9.2

RUN export DEBIAN_FRONTEND='noninteractive' && \
    apt-get update && apt-get upgrade -y  && \
    apt-get install -y screen git mercurial python-dulwich wget rsync net-tools coreutils

USER root
RUN useradd -ms /bin/bash sia
USER sia

ENV work	/home/sia/work
RUN mkdir -p ${work}
ENV gopath	/home/sia/work/go
RUN mkdir -p ${gopath}
ENV profile  /home/sia/.profile
RUN echo "GOPATH=${gopath}; export GOPATH" >> ${profile}

WORKDIR /home/sia

COPY clone.bash .
RUN bash -x ./clone.bash

COPY go-build.bash .
RUN bash -x ./go-build.bash

# Sia API
EXPOSE 9980
# Sia gateway for incoming peers
EXPOSE 9981
# Sia host for rentals
EXPOSE 9982

ENV sia_d       /home/sia/var/sia.d/
RUN mkdir -p ${sia_d}

ENV var_tmp    /home/sia/var/tmp/
RUN mkdir -p ${var_tmp}

# Create subdirectories owned by sia user
RUN for dir in consensus gateway host miner renter transactionpool wallet; do \
      mkdir -p ${sia_d}/${dir}; \
   done

# Create <file:http-api-secret> with a password for the siad api
COPY ./http-api-secret  /home/sia/http-api-secret

CMD /home/sia/work/go/bin/siad \
  --sia-directory ${sia_d} \
  --modules cghmrtw \
  --authenticate-api \
  < /home/sia/http-api-secret  



