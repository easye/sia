#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

log_d=$HOME/var/log; mkdir -p $log_d
var_sia_d=$HOME/var/sia.d; mkdir -p $var_sia_d

if [[ ! -f $DIR/http-api-secret ]]; then
    echo No $DIR/http-api-secret present.  Please create one.
    exit 1

fi

cmd=$GOPATH/bin/siad
echo Backgrounding $cmd with data in ${var_sia_d} logging to ${log_d}

exec $cmd \
    --modules cghmrtw \
    --sia-directory ${var_sia_d} \
    --authenticate-api \
  >> $log_d/siad.out 2>&1 \
   < $DIR/http-api-secret &                     

#$GOPATH/bin/siad --sia-directory $var_sia_d --authenticate-api >> $log_dir//siad.out 2>&1 < $HOME/work/easye/sia/http-api-secret

