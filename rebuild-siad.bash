#!/usr/bin/env bash

# assuming this is correct
GOPATH=$HOME/work/go

gobin_d=$GOPATH/bin
siad=$gobin_d/siad
siac=$gobin_d/siac

stamp=$(date "+%Y%m%d%H%M.%S")
for file in $siad $siac; do
    cp $file ${file}.${stamp}
done

go get -u -x github.com/NebulousLabs/Sia/...


