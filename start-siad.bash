#!/bin/bash

if [[ -z $1 ]]; then
    image=easye/sia:latest
else
    image=$1
fi

host_d=$(pwd)/var
sia_d=/var/local/sia.d

mkdir -p ${host_d}

docker run --detach --name sia \
   --publish 9980:9980 --publish 9981:9981 --publish 9982:9982 \
   "--volume=${host_d}:${sia_d}" \
   "--volume=${host_d}/consensus:${sia_d}/consensus" \
   "--volume=${host_d}/wallet:${sia_d}/wallet" \
   "--volume=${host_d}/renter:${sia_d}/renter" \
   "--volume=${host_d}/miner:${sia_d}/miner" \
   "--volume=${host_d}/transactionpool:${sia_d}/transactionpool" \
   "--volume=${host_d}/host:${sia_d}/host" \
   "--volume=${host_d}/gateway:${sia_d}/gateway" \
 $image
