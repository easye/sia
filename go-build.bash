#!/usr/bin/env bash

profile=~/.profile
work=~/work/
gopath=${work}/go/

. ${profile} \
 && cd ${gopath}/src/github.com/NebulousLabs/Sia/ \
 && go get -d -x ...

. ${profile} \
 && cd ${gopath}/src/github.com/NebulousLabs/Sia/ \
 && go install -x ./siac ./siad
