DOCKER	:= docker

.PHONY: all 
all: generate-secret 
	$(DOCKER) build -t easye/sia .

APG	:= apg
SEED=s3cr3t
.PHONY: generate-secret
generate-secret: http-api-secret
http-api-secret:
	echo "Regenerating <file:http-api-secret>..."
	bash -x ./generate-secret.bash

.PHONY:
clean:
	rm http-api-secret
